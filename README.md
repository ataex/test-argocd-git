# Test Argocd Git

## Context

Delivering apps in a simple, repeatable modular flow is an art in itself. A lot of concepts and tools try to make life easier:

- GitLab is a web-based software project lifecycle management platform that provides version control, continuous integration, issue tracking, and more.
- ArgoCD is a declarative, GitOps continuous delivery tool for Kubernetes that automates the deployment and synchronization of applications and their configurations.
- Kubernetes is an open-source container orchestration platform that automates the deployment, scaling, and management of containerized applications.

While working on a service tool & api to deliver customized wordpress instances for end users via using wrapped helm cli I got an idea.

We (Arne&Justus) got in contact with ArgoCD and the "babysitter" pattern at the KCD Munich 2023. Through that I questioned if it would be easier to implement the above project with reusing gitlab and argocd mainly as backend components meaning that by using git repo manipulation and basic ArgoCD API you could create a plattform to deliver open source apps or even own or private third party apps on Kubernetes Cluster.

Basicly building a simple to use Web interface & little service on top of a git sdk and ArgoCD API  

## Using / Testing it

To test it out you need:

- go: 1.16+
- kubectl
- podman <3 or docker
- kind: https://kind.sigs.k8s.io/
    - go install sigs.k8s.io/kind@v0.20.0 
    - move kind to ~/bin
    - kind create cluster 

Used then: https://shashanksrivastava.medium.com/install-configure-argo-cd-on-kind-kubernetes-cluster-f0fee69e5ac4

- kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

- kubectl port-forward -n argocd service/argocd-server 8080:443

- kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d && echo

- Via localhost:8080 you can then login it ArgoCD. Username: admin Password: See above command

- Then you can create an application. (currently the application.yaml is not useable).
    - PROJECT: default
    - CLUSTER: https://kubernetes.default.svc
    - Namespace: default
    - REPO URL: https://gitlab.com/ataex/test-argocd-git.git
    - TARGET REVISION: HEAD
    - PATH: apps
    - SYNC AUTOMATED
    - PRUNE RESOURCES
    - SELF HEAL

- You can then check gitlab repo and what ArgoCD UI is showing you and also can check via kubectl

## PLAY

What do I mean by that? 

You can now delete deployments & services of a app like nginx and push it. Then you can wait till ArgoCD Controller detects the change and is adjusting the pods etc. or you can click on sync in ArgoCD UI

## Imagine

Now imagine that you have an UI interface and a lot of the manual steps are exchanged by automatic steps.

For example: You click on a deploy wordpress, then you get a form to get informations for the deployment like domain, name, login data etc. and then by clicking a button the stuff is stored in gitlab, argocd api is triggered and the stuff is then deployed.
